module.exports  = {
	auth : [
	{act: true, user:'test', pass:'test', id:'DEV1-00000000', descr: 'test device'},
	{act: true, user:'test2', pass:'test2', id:'DEV1-00000000', descr: 'test device'},
	],
	http: {
		port: 8686,
		webui_path: '/WEBUI',
	},
	mqtt : {
		url:'mqtt://broker.ru',
		sys_name: 'SYSTEM',
		subsys_name: 'GROUP',
		username: 'username',
		userpass: 'password',
	timeout: 30000,
	}

}
