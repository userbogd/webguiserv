http = require("http"),
url = require("url"),
path = require("path"),
fs = require("fs")
mqtt = require("mqtt");
const config = require('./config.js');

const MQTT_TIMEOUT = config.mqtt.timeout;
const MQTT_URL = config.mqtt.url;
const MQTT_USERNAME = config.mqtt.username; 
const MQTT_PASSWORD = config.mqtt.userpass;
const MQTT_SYSNAME = config.mqtt.sys_name;
const MQTT_SUBSYSNAME = config.mqtt.subsys_name;
const HTTP_WEBUI_PATH = config.http.webui_path;
const HTTP_PORT = config.http.port;

let MQTT_DEVICE_RX = '';
let MQTT_DEVICE_TX = MQTT_SYSNAME + '/' + MQTT_SUBSYSNAME + '/+/SYSTEM/UPLINK';

mimeTypes = {
  "html": "text/html",
  "jpeg": "image/jpeg",
  "jpg": "image/jpeg",
  "png": "image/png",
  "svg": "image/svg+xml",
  "json": "application/json",
  "js": "text/javascript",
  "css": "text/css"
};

let cache_list = []; 
function auth3(lg, pwd)
{
  var id = null;
  config.auth.forEach((cred) => {
  if(cred.act&&
     cred.user == lg && 
     cred.pass == pwd){
    id = cred.id;
  }});
  return id;
};

http.createServer(function(request, response) {
  var header = request.headers.authorization || '';       
  var token = header.split(/\s+/).pop() || '';       
  var auth = Buffer.from(token, 'base64').toString(); 
  var parts = auth.split(/:/);                        
  var username = parts.shift();                      
  var password = parts.join(':'); 
  let ID = null;
  ID = auth3(username, password);
  if( ID == null)
  {
    response.writeHead(401, { 'WWW-Authenticate': 'Basic realm="nope"' });
    response.end('HTTP Error 401 Unauthorized: Access is denied');
    return;
  }
  MQTT_DEVICE_RX = MQTT_SYSNAME + '/' + MQTT_SUBSYSNAME+ '/' + ID +'/SYSTEM/DWLINK';

  if(request.method == 'GET'){
  
  var uri = url.parse(request.url).pathname, 
      filename = path.join(process.cwd() + HTTP_WEBUI_PATH, uri);
  
  fs.exists(filename, function(exists) {
    if(!exists) {
      response.writeHead(404, { "Content-Type": "text/plain" });
      response.write("404 Not Found\n");
      response.end();
      return;
    }
 
    if (fs.statSync(filename).isDirectory()) 
      filename += '/index.html';

    fs.readFile(filename, "binary", function(err, file) {
      if(err) {        
        response.writeHead(500, {"Content-Type": "text/plain"});
        response.write(err + "\n");
        response.end();
        return;
      }

      var mimeType = mimeTypes[filename.split('.').pop()];
      
      if (!mimeType) {
        mimeType = 'text/plain';
      }
      //response.writeHead(200, { "Content-Type": mimeType});
      response.writeHead(200, { "Content-Type": mimeType, "Content-Encoding": "gzip"});
      response.write(file, "binary");
      response.end();
    });
  });
}
else if (request.method == 'POST'){
  if (request.url == '/api'){
  var body = '';  
  request.on('data', function(data) {
    body += data; 
  })
  request.on ('end', function() {
    //Publising request and pending return handler
    client.publish(MQTT_DEVICE_RX, body, { qos:1 });
    response.writeHead(200, {"Content-Type": "application"});    
    onDataBack = function(message){         
    response.write(message);
    response.end();
    return;}
    let message_id = null;
    try{message_id = JSON.parse(body).data.msgid;}
    catch{}
    cache_list.push({
      ts: Date.now(),
      topic: MQTT_DEVICE_RX.replace('DWLINK', 'UPLINK'),  
      msgid: message_id, 
      handler: onDataBack,
      handled: false
    });
  })
  }
  else{
    response.writeHead(404, { "Content-Type": "text/plain" });
    response.write("404 Not Found\n");
    response.end();
    return;
  }
}

}).listen (parseInt(HTTP_PORT, 10));
console.log("HTTP service started on port " + HTTP_PORT);
console.log("HTTP webui path " + HTTP_WEBUI_PATH);

//MQTT service
var client;
client = mqtt.connect(MQTT_URL, { username: MQTT_USERNAME , password: MQTT_PASSWORD});
client.on("connect", () => {
  console.log("MQTT transport connected to "+ MQTT_URL);
  client.subscribe(MQTT_DEVICE_TX, (err) => {
    console.log("MQTT subscribed to " + MQTT_DEVICE_TX );
  });
});

client.on("message", (topic, message) => {
  msgid = null;
  try{ msgid = JSON.parse(message).data.msgid }
  catch{}
  cache_list.forEach((pend) => {
  //Checking return topic and message id and execute stored callback  
  if( topic == pend.topic && (!msgid || msgid == pend.msgid))
  {
    if(pend.handler){
      pend.handler(message);
      pend.handled = true;
    }
  }})
});


setInterval(() => {
    cur_ts = Date.now();
    cache_list.forEach((rec) => {
      if(cur_ts - rec.ts >= MQTT_TIMEOUT)
      {
        if(rec.handler){  
          rec.handler("{error:\"DEVICE_NOT_RESPONSE\"}");
          rec.handled = true;
        }
      }  
    const cleared = cache_list.filter(it => it.handled != true);
    cache_list = cleared;
    })
  //console.log("Cache_list size "+ cache_list.length);
  }, 1000);


